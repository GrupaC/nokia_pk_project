#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include "Application.hpp"
#include "Mocks/ILoggerMock.hpp"
#include "Mocks/IBtsPortMock.hpp"
#include "Mocks/IUserPortMock.hpp"
#include "Mocks/ITimerPortMock.hpp"
#include "Messages/PhoneNumber.hpp"
#include <memory>

namespace ue
{
using namespace ::testing;

class ApplicationTestSuite : public Test
{
protected:
    const common::PhoneNumber PHONE_NUMBER{112}, PHONE_NUMBER2{113};
    const common::BtsId BTS_ID{13121981ll};
    const std::string TEXT{"message"};
    NiceMock<common::ILoggerMock> loggerMock;
    StrictMock<IBtsPortMock> btsPortMock;
    StrictMock<IUserPortMock> userPortMock;
    StrictMock<ITimerPortMock> timerPortMock;

    Expectation expectShowNotConnected = EXPECT_CALL(userPortMock, showNotConnected());
    Application objectUnderTest{PHONE_NUMBER,
                                loggerMock,
                                btsPortMock,
                                userPortMock,
                                timerPortMock};
};

struct ApplicationNotConnectedTestSuite : ApplicationTestSuite
{
    void handleSib()
    {
        EXPECT_CALL(btsPortMock, sendAttachRequest(BTS_ID));
        EXPECT_CALL(userPortMock, showConnecting());
        EXPECT_CALL(timerPortMock, startAttachTimer());
        objectUnderTest.handleSib(BTS_ID);
    }
};

TEST_F(ApplicationNotConnectedTestSuite, shallShowNotConnected)
{
}

TEST_F(ApplicationNotConnectedTestSuite, shallHandleSib)
{
    handleSib();
}

struct ApplicationConnectingTestSuite : ApplicationNotConnectedTestSuite
{
    ApplicationConnectingTestSuite()
    {
        handleSib();
    }
    void handleAttachAccept()
    {
        EXPECT_CALL(userPortMock, showConnected());
        EXPECT_CALL(timerPortMock, stopAttachTimer());
        objectUnderTest.handleAttachAccept();
    }

    void handleDisconnected()
    {
        EXPECT_CALL(userPortMock, showNotConnected());
        EXPECT_CALL(timerPortMock, stopAttachTimer());
        objectUnderTest.handleDisconnected();
    }
};

TEST_F(ApplicationConnectingTestSuite, shallHandleAttachAccept)
{
    handleAttachAccept();
}

TEST_F(ApplicationConnectingTestSuite, shallHandleAttachReject)
{
    EXPECT_CALL(userPortMock, showNotConnected());
    EXPECT_CALL(timerPortMock, stopAttachTimer());
    objectUnderTest.handleAttachReject();
}

TEST_F(ApplicationConnectingTestSuite, shallHandleAttachTimeout)
{
    EXPECT_CALL(userPortMock, showNotConnected());
    objectUnderTest.handleAttachTimeout();
}

TEST_F(ApplicationConnectingTestSuite, shallHandleDisconnected)
{
    handleDisconnected();
}

struct ApplicationConnectedTestSuite : ApplicationConnectingTestSuite
{
    ApplicationConnectedTestSuite(){
        handleAttachAccept();
    }
    void handleDisconnected()
    {
        EXPECT_CALL(userPortMock, showNotConnected());
        objectUnderTest.handleDisconnected();
    }

    void handleCallAccept()
    {
        EXPECT_CALL(userPortMock, showCallMode(PHONE_NUMBER));
        EXPECT_CALL(timerPortMock, stopOutgoingCallTimer());
        objectUnderTest.handleCallAccept(PHONE_NUMBER);
    }
};

TEST_F(ApplicationConnectedTestSuite, shallHandleDisconnected)
{
    handleDisconnected();
}

TEST_F(ApplicationConnectedTestSuite, shallHandleSmsReceive)
{
    EXPECT_CALL(userPortMock, addReceivedSms(PHONE_NUMBER, TEXT));
    EXPECT_CALL(userPortMock, showSmsNotification());
    objectUnderTest.handleSmsReceive(PHONE_NUMBER, TEXT);
}

TEST_F(ApplicationConnectedTestSuite, shallHandleSmsSend)
{
    EXPECT_CALL(btsPortMock, sendSms(PHONE_NUMBER2,TEXT));
    objectUnderTest.handleSmsSend(PHONE_NUMBER2, TEXT);
}

TEST_F(ApplicationConnectedTestSuite, shallhandleUnknownRecipientOfSms)
{
    EXPECT_CALL(userPortMock, markSmsAsFailed(PHONE_NUMBER2));
    objectUnderTest.handleUnknownRecipientOfSms(PHONE_NUMBER2);
}

TEST_F(ApplicationConnectedTestSuite, shallhandleCallAccept)
{
    handleCallAccept();
}

TEST_F(ApplicationConnectedTestSuite, shallHandleCallRequestSend)
{
    EXPECT_CALL(btsPortMock, sendCallRequest(PHONE_NUMBER2));
    EXPECT_CALL(timerPortMock, startOutgoingCallTimer());
    objectUnderTest.handleCallRequestSend(PHONE_NUMBER2);
}

TEST_F(ApplicationConnectedTestSuite, shallHandleCallRequest)
{
    EXPECT_CALL(userPortMock, showIncomingCall(PHONE_NUMBER2));
    EXPECT_CALL(timerPortMock, startIncomingCallTimer());
    objectUnderTest.handleCallRequest(PHONE_NUMBER2);
}

TEST_F(ApplicationConnectedTestSuite, shallHandleIncomingCallDrop)
{
    EXPECT_CALL(timerPortMock, stopIncomingCallTimer());
    EXPECT_CALL(btsPortMock, dropCall(PHONE_NUMBER2));
    objectUnderTest.handleIncomingCallDrop(PHONE_NUMBER2);
}

TEST_F(ApplicationConnectedTestSuite, shallHandleIncomingCallAccept)
{
    EXPECT_CALL(timerPortMock, stopIncomingCallTimer());
    EXPECT_CALL(btsPortMock, acceptCall(PHONE_NUMBER2));
    EXPECT_CALL(userPortMock, showCallMode(PHONE_NUMBER2));
    objectUnderTest.handleIncomingCallAccept(PHONE_NUMBER2);
}

TEST_F(ApplicationConnectedTestSuite, shallHandleIncomingCallTimeout){
    EXPECT_CALL(userPortMock, showConnected());
    EXPECT_CALL(userPortMock, getCallPartnerNumber()).WillOnce(Return(PHONE_NUMBER2));
    EXPECT_CALL(btsPortMock, dropCall(PHONE_NUMBER2));
    objectUnderTest.handleIncomingCallTimeout();
}

TEST_F(ApplicationConnectedTestSuite, shallHandleOutgoingCallTimeout){
    EXPECT_CALL(userPortMock, showConnected());
    EXPECT_CALL(userPortMock, getCallPartnerNumber()).WillOnce(Return(PHONE_NUMBER2));
    EXPECT_CALL(btsPortMock, dropCall(PHONE_NUMBER2));
    objectUnderTest.handleOutgoingCallTimeout();
}

TEST_F(ApplicationConnectedTestSuite, shallHandleUnknownRecipientOfCall)
{
    EXPECT_CALL(timerPortMock, startUnknownRecipientTimer());
    EXPECT_CALL(userPortMock, showUnknownRecipientOfCall(_));
    objectUnderTest.handleUnknownRecipientOfCall(PHONE_NUMBER2);
}

TEST_F(ApplicationConnectedTestSuite, shallHandleOutgoingCallDrop)
{
    EXPECT_CALL(timerPortMock, stopIncomingCallTimer());
    EXPECT_CALL(btsPortMock, dropCall(PHONE_NUMBER2));
    objectUnderTest.handleOutgoingCallDrop(PHONE_NUMBER2);
}

TEST_F(ApplicationConnectedTestSuite, shallHandleUnknownRecipientTimeout)
{
    EXPECT_CALL(userPortMock, showConnected());
    objectUnderTest.handleUnknownRecipientTimeout();
}

struct ApplicationTalkingTestSuite : ApplicationConnectedTestSuite
{
    ApplicationTalkingTestSuite()
    {
        handleCallAccept();
    }
};

TEST_F(ApplicationTalkingTestSuite, shallHandleUnknownRecipientOfCall)
{
    EXPECT_CALL(timerPortMock, startUnknownRecipientTimer());
    EXPECT_CALL(userPortMock, showUnknownRecipientOfCall(_));
    objectUnderTest.handleUnknownRecipientOfCall(PHONE_NUMBER2);
}

TEST_F(ApplicationTalkingTestSuite, shallHandleUnknownRecipientTimeout)
{
    EXPECT_CALL(userPortMock, showConnected());
    objectUnderTest.handleUnknownRecipientTimeout();
}

TEST_F(ApplicationTalkingTestSuite, shallHandleCancelUnknownRepcipientShow)
{
    EXPECT_CALL(userPortMock, showConnected());
    EXPECT_CALL(timerPortMock, stopUnknownRecipientTimer());
    objectUnderTest.handleCancelUnknownRecipientShow();
}

TEST_F(ApplicationTalkingTestSuite, shallHandleUserEndCall)
{
    EXPECT_CALL(userPortMock, showConnected());
    EXPECT_CALL(timerPortMock, stopTalkingTimer());
    EXPECT_CALL(btsPortMock, dropCall(PHONE_NUMBER2));
    objectUnderTest.handleUserEndCall(PHONE_NUMBER2);
}

TEST_F(ApplicationTalkingTestSuite, shallHandleCallDrop)
{
    EXPECT_CALL(userPortMock, showConnected());
    EXPECT_CALL(timerPortMock, stopTalkingTimer());
    objectUnderTest.handleCallDrop(PHONE_NUMBER2);
}

TEST_F(ApplicationTalkingTestSuite, shallHandleCallTalkSend)
{
    EXPECT_CALL(timerPortMock, stopTalkingTimer());
    EXPECT_CALL(timerPortMock, startTalkingTimer());
    EXPECT_CALL(btsPortMock, sendCallTalk(PHONE_NUMBER2, TEXT));
    objectUnderTest.handleCallTalkSend(PHONE_NUMBER2, TEXT);
}

TEST_F(ApplicationTalkingTestSuite, shallHandleCallTalkReceive)
{
    EXPECT_CALL(timerPortMock, stopTalkingTimer());
    EXPECT_CALL(timerPortMock, startTalkingTimer());
    EXPECT_CALL(userPortMock, showIncomingCallTalk(PHONE_NUMBER2, TEXT));
    objectUnderTest.handleCallTalkReceive(PHONE_NUMBER2, TEXT);
}

TEST_F(ApplicationTalkingTestSuite, shallHandleTalkingTimeout)
{
    common::PhoneNumber callPartnerNumberMock;
    EXPECT_CALL(userPortMock, getCallPartnerNumber()).WillOnce(Return(callPartnerNumberMock));
    EXPECT_CALL(btsPortMock, dropCall(callPartnerNumberMock));
    EXPECT_CALL(userPortMock, showConnected());
    objectUnderTest.handleTalkingTimeout();
}

}
