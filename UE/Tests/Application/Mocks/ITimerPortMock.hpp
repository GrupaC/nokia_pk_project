#pragma once

#include <gmock/gmock.h>
#include "Ports/ITimerPort.hpp"

namespace ue
{

class ITimerEventsHandlerMock : public ITimerEventsHandler
{
public:
    ITimerEventsHandlerMock();
    ~ITimerEventsHandlerMock() override;

#define TIMEOUT_MOCK_METHODS(X) \
    MOCK_METHOD0(handle##X##Timeout, void());

    FOR_ALL_TIMERS(TIMEOUT_MOCK_METHODS)

#undef TIMEOUT_MOCK_METHODS
};

class ITimerPortMock : public ITimerPort
{
public:
    ITimerPortMock();
    ~ITimerPortMock() override;

#define TIMER_MOCK_START_STOP_METHODS(X)    \
    MOCK_METHOD0(start##X##Timer, void());  \
    MOCK_METHOD0(stop##X##Timer, void());

    FOR_ALL_TIMERS(TIMER_MOCK_START_STOP_METHODS)

#undef TIMER_MOCK_START_STOP_METHODS
};

}
