#pragma once

#include <gmock/gmock.h>
#include "Ports/IUserPort.hpp"

namespace ue
{

class IUserEventsHandlerMock : public IUserEventsHandler
{
public:
    IUserEventsHandlerMock();
    ~IUserEventsHandlerMock() override;
    MOCK_METHOD2(handleSmsSend, void(common::PhoneNumber, const std::string&));
    MOCK_METHOD1(handleCallRequestSend, void(common::PhoneNumber));
    MOCK_METHOD1(handleIncomingCallDrop, void(common::PhoneNumber));
    MOCK_METHOD1(handleIncomingCallAccept, void(common::PhoneNumber));
    MOCK_METHOD0(handleCancelUnknownRecipientShow, void());
    MOCK_METHOD1(handleOutgoingCallDrop, void(common::PhoneNumber));
    MOCK_METHOD1(handleUserEndCall, void(common::PhoneNumber));
    MOCK_METHOD2(handleCallTalkSend, void(common::PhoneNumber, const std::string&));
};

class IUserPortMock : public IUserPort
{
public:
    IUserPortMock();
    ~IUserPortMock() override;

    MOCK_METHOD0(showNotConnected, void());
    MOCK_METHOD0(showConnecting, void());
    MOCK_METHOD0(showConnected, void());
    MOCK_METHOD0(showSmsNotification, void());
    MOCK_METHOD1(markSmsAsFailed, void(common::PhoneNumber));
    MOCK_METHOD2(addReceivedSms, void(common::PhoneNumber, const std::string&));
    MOCK_METHOD1(showCallMode, void(common::PhoneNumber));
    MOCK_METHOD1(showIncomingCall, void(common::PhoneNumber));
    MOCK_METHOD1(showOutgoingCall, void(common::PhoneNumber));
    MOCK_METHOD1(showUnknownRecipientOfCall, void(common::PhoneNumber));
    MOCK_METHOD2(showIncomingCallTalk, void(common::PhoneNumber, const std::string&));
    MOCK_METHOD0(getCallPartnerNumber, common::PhoneNumber());
};

}
