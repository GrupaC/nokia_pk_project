#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include "Ports/BtsPort.hpp"
#include "Mocks/ILoggerMock.hpp"
#include "Mocks/IBtsPortMock.hpp"
#include "Messages/PhoneNumber.hpp"
#include "Mocks/ITransportMock.hpp"
#include "Messages/OutgoingMessage.hpp"
#include "Messages/IncomingMessage.hpp"

namespace ue
{
using namespace ::testing;

class BtsPortTestSuite : public Test
{
protected:
    const common::PhoneNumber PHONE_NUMBER{112}, PHONE_NUMBER2{113};
    const common::BtsId BTS_ID{13121981ll};
    const std::string TEXT{"message"};
    NiceMock<common::ILoggerMock> loggerMock;
    StrictMock<IBtsEventsHandlerMock> handlerMock;
    StrictMock<common::ITransportMock> transportMock;
    common::ITransport::MessageCallback messageCallback;
    common::ITransport::DisconnectedCallback disconnectedCallback;

    BtsPort objectUnderTest{loggerMock, transportMock, PHONE_NUMBER};

    BtsPortTestSuite()
    {
        EXPECT_CALL(transportMock, registerMessageCallback(_))
                .WillOnce(SaveArg<0>(&messageCallback));
        EXPECT_CALL(transportMock, registerDisconnectedCallback(_))
                        .WillOnce(SaveArg<0>(&disconnectedCallback));
        objectUnderTest.start(handlerMock);
    }
    ~BtsPortTestSuite()
    {

        EXPECT_CALL(transportMock, registerMessageCallback(IsNull()));
        EXPECT_CALL(transportMock, registerDisconnectedCallback(IsNull()));
        objectUnderTest.stop();
    }
};

TEST_F(BtsPortTestSuite, shallRegisterHandlersBetweenStartStop)
{
}

TEST_F(BtsPortTestSuite, shallIgnoreWrongMessage)
{
    common::OutgoingMessage wrongMsg{};
    wrongMsg.writeBtsId(BTS_ID);
    messageCallback(wrongMsg.getMessage());
}

TEST_F(BtsPortTestSuite, shallHandleSib)
{
    EXPECT_CALL(handlerMock, handleSib(BTS_ID));
    common::OutgoingMessage msg{common::MessageId::Sib,
                                common::PhoneNumber{},
                                PHONE_NUMBER};
    msg.writeBtsId(BTS_ID);
    messageCallback(msg.getMessage());
}

TEST_F(BtsPortTestSuite, shallHandleAttachAccept)
{
    EXPECT_CALL(handlerMock, handleAttachAccept());
    common::OutgoingMessage msg{common::MessageId::AttachResponse,
                                common::PhoneNumber{},
                                PHONE_NUMBER};
    msg.writeNumber(true);
    messageCallback(msg.getMessage());
}

TEST_F(BtsPortTestSuite, shallHandleAttachReject)
{
    EXPECT_CALL(handlerMock, handleAttachReject());
    common::OutgoingMessage msg{common::MessageId::AttachResponse,
                                common::PhoneNumber{},
                                PHONE_NUMBER};
    msg.writeNumber(false);
    messageCallback(msg.getMessage());
}

TEST_F(BtsPortTestSuite, shallHandleUnknownRecipientOfSms)
{
    EXPECT_CALL(handlerMock, handleUnknownRecipientOfSms(_));
    common::OutgoingMessage msg{common::MessageId::UnknownRecipient,
                                common::PhoneNumber{},
                                PHONE_NUMBER};
    msg.writeMessageId(common::MessageId::Sms);
    msg.writePhoneNumber(PHONE_NUMBER);
    msg.writePhoneNumber(PHONE_NUMBER2);
    msg.writeText(TEXT);
    messageCallback(msg.getMessage());
}

TEST_F(BtsPortTestSuite, shallSendAttachRequest)
{
    common::BinaryMessage msg;
    EXPECT_CALL(transportMock, sendMessage(_)).WillOnce(SaveArg<0>(&msg));
    objectUnderTest.sendAttachRequest(BTS_ID);
    common::IncomingMessage reader(msg);
    ASSERT_NO_THROW(EXPECT_EQ(common::MessageId::AttachRequest, reader.readMessageId()) );
    ASSERT_NO_THROW(EXPECT_EQ(PHONE_NUMBER, reader.readPhoneNumber()));
    ASSERT_NO_THROW(EXPECT_EQ(common::PhoneNumber{}, reader.readPhoneNumber()));
    ASSERT_NO_THROW(EXPECT_EQ(BTS_ID, reader.readBtsId()));
    ASSERT_NO_THROW(reader.checkEndOfMessage());
}

TEST_F(BtsPortTestSuite, shallSendSms)
{
    common::BinaryMessage msg;
    EXPECT_CALL(transportMock, sendMessage(_)).WillOnce(SaveArg<0>(&msg));
    objectUnderTest.sendSms(PHONE_NUMBER2, TEXT);
    common::IncomingMessage reader(msg);
    ASSERT_NO_THROW(EXPECT_EQ(common::MessageId::Sms, reader.readMessageId()));
    ASSERT_NO_THROW(EXPECT_EQ(PHONE_NUMBER, reader.readPhoneNumber()));
    ASSERT_NO_THROW(EXPECT_EQ(PHONE_NUMBER2, reader.readPhoneNumber()));
    std::uint8_t ENCRYPTION_TYPE = reader.readNumber<std::uint8_t>();
    ASSERT_NO_THROW(EXPECT_GE(ENCRYPTION_TYPE, 0));
    ASSERT_NO_THROW(EXPECT_LE(ENCRYPTION_TYPE, 3));
    ASSERT_NO_THROW(EXPECT_EQ(TEXT, reader.readText(TEXT.size())));
    ASSERT_NO_THROW(reader.checkEndOfMessage());
}

TEST_F(BtsPortTestSuite, shallHandleDisconnectedCallback)
{
    EXPECT_CALL(handlerMock, handleDisconnected());
    disconnectedCallback();
}

TEST_F(BtsPortTestSuite, shallHandleSmsReceive)
{
    common::OutgoingMessage msg{common::MessageId::Sms,
                                common::PhoneNumber{},
                                PHONE_NUMBER};
    msg.writeText("test");
    EXPECT_CALL(handlerMock, handleSmsReceive(_, _));
    messageCallback(msg.getMessage());
}

TEST_F(BtsPortTestSuite, shallSendCallRequest)
{
    common::BinaryMessage msg;
    EXPECT_CALL(transportMock, sendMessage(_)).WillOnce(SaveArg<0>(&msg));
    objectUnderTest.sendCallRequest(PHONE_NUMBER2);
    common::IncomingMessage reader(msg);
    ASSERT_NO_THROW(EXPECT_EQ(common::MessageId::CallRequest, reader.readMessageId()));
    ASSERT_NO_THROW(EXPECT_EQ(PHONE_NUMBER, reader.readPhoneNumber()));
    ASSERT_NO_THROW(EXPECT_EQ(PHONE_NUMBER2, reader.readPhoneNumber()));
    std::uint8_t ENCRYPTION_TYPE = reader.readNumber<std::uint8_t>();
    ASSERT_NO_THROW(EXPECT_GE(ENCRYPTION_TYPE, 0));
    ASSERT_NO_THROW(EXPECT_LE(ENCRYPTION_TYPE, 3));
    ASSERT_NO_THROW(reader.checkEndOfMessage());
}

TEST_F(BtsPortTestSuite, shallHandleCallRequest)
{
    EXPECT_CALL(handlerMock, handleCallRequest(PHONE_NUMBER2));
    common::OutgoingMessage msg{common::MessageId::CallRequest,
                                PHONE_NUMBER2,
                                PHONE_NUMBER};
    msg.writeNumber<std::uint8_t>(0);
    messageCallback(msg.getMessage());
}

TEST_F(BtsPortTestSuite, shallHandleCallAccept)
{
    EXPECT_CALL(handlerMock, handleCallAccept(PHONE_NUMBER2));
    common::OutgoingMessage msg{common::MessageId::CallAccepted,
                               PHONE_NUMBER2,
                               PHONE_NUMBER};
    msg.writeNumber<std::uint8_t>(0);
    messageCallback(msg.getMessage());
}
TEST_F(BtsPortTestSuite, shalHandleCallDrop)
{
    EXPECT_CALL(handlerMock, handleCallDrop(PHONE_NUMBER2));
    common::OutgoingMessage msg{
        common::MessageId::CallDropped,
                PHONE_NUMBER2,
                PHONE_NUMBER
    };
    msg.writeNumber<std::uint8_t>(0);
    messageCallback(msg.getMessage());
}
TEST_F(BtsPortTestSuite, shallDropCall)
{
    common::BinaryMessage msg;
    EXPECT_CALL(transportMock, sendMessage(_)).WillOnce(SaveArg<0>(&msg));
    objectUnderTest.dropCall(PHONE_NUMBER2);
    common::IncomingMessage reader(msg);
    ASSERT_NO_THROW(EXPECT_EQ(common::MessageId::CallDropped, reader.readMessageId()));
    ASSERT_NO_THROW(EXPECT_EQ(PHONE_NUMBER, reader.readPhoneNumber()));
    ASSERT_NO_THROW(EXPECT_EQ(PHONE_NUMBER2, reader.readPhoneNumber()));
    ASSERT_NO_THROW(reader.checkEndOfMessage());
}

TEST_F(BtsPortTestSuite, shallAcceptCall)
{
    common::BinaryMessage msg;
    EXPECT_CALL(transportMock, sendMessage(_)).WillOnce(SaveArg<0>(&msg));
    objectUnderTest.acceptCall(PHONE_NUMBER2);
    common::IncomingMessage reader(msg);
    ASSERT_NO_THROW(EXPECT_EQ(common::MessageId::CallAccepted, reader.readMessageId()));
    ASSERT_NO_THROW(EXPECT_EQ(PHONE_NUMBER, reader.readPhoneNumber()));
    ASSERT_NO_THROW(EXPECT_EQ(PHONE_NUMBER2, reader.readPhoneNumber()));
    ASSERT_NO_THROW(reader.checkEndOfMessage());
}

TEST_F(BtsPortTestSuite, shallHandleUnknownRecipientOfCallAcctepted)
{
    EXPECT_CALL(handlerMock, handleUnknownRecipientOfCall(_));
    common::OutgoingMessage msg{common::MessageId::UnknownRecipient,
                                common::PhoneNumber{},
                                PHONE_NUMBER};
    msg.writeMessageId(common::MessageId::CallAccepted);
    msg.writePhoneNumber(PHONE_NUMBER);
    msg.writePhoneNumber(PHONE_NUMBER2);
    messageCallback(msg.getMessage());
}

TEST_F(BtsPortTestSuite, shallHandleUnknownRecipientOfCallDropped)
{
    EXPECT_CALL(handlerMock, handleUnknownRecipientOfCall(_));
    common::OutgoingMessage msg{common::MessageId::UnknownRecipient,
                                common::PhoneNumber{},
                                PHONE_NUMBER};
    msg.writeMessageId(common::MessageId::CallDropped);
    msg.writePhoneNumber(PHONE_NUMBER);
    msg.writePhoneNumber(PHONE_NUMBER2);
    messageCallback(msg.getMessage());
}

TEST_F(BtsPortTestSuite, shallSendCallTalk)
{
    common::BinaryMessage msg;
    EXPECT_CALL(transportMock, sendMessage(_)).WillOnce(SaveArg<0>(&msg));
    objectUnderTest.sendCallTalk(PHONE_NUMBER2, TEXT);
    common::IncomingMessage reader(msg);
    ASSERT_NO_THROW(EXPECT_EQ(common::MessageId::CallTalk, reader.readMessageId()));
    ASSERT_NO_THROW(EXPECT_EQ(PHONE_NUMBER, reader.readPhoneNumber()));
    ASSERT_NO_THROW(EXPECT_EQ(PHONE_NUMBER2, reader.readPhoneNumber()));
    ASSERT_NO_THROW(EXPECT_EQ(TEXT, reader.readText(TEXT.size())));
    ASSERT_NO_THROW(reader.checkEndOfMessage());
}

TEST_F(BtsPortTestSuite, shallHandleCallTalkReceive)
{
    common::OutgoingMessage msg{common::MessageId::CallTalk,
                                PHONE_NUMBER2,
                                PHONE_NUMBER};
    msg.writeText("test");
    EXPECT_CALL(handlerMock, handleCallTalkReceive(PHONE_NUMBER2, "test"));
    messageCallback(msg.getMessage());
}

}
