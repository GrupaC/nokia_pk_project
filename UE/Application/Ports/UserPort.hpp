#pragma once

#include "IUserPort.hpp"
#include "Logger/PrefixedLogger.hpp"
#include "IUeGui.hpp"
#include "Messages/PhoneNumber.hpp"
#include "Sms/ISmsDb.hpp"
#include "Sms/SmsService.hpp"

namespace ue
{

class UserPort : public IUserPort
{
public:
    UserPort(common::ILogger& logger, IUeGui& gui, common::PhoneNumber phoneNumber, ISmsDb& smsDb);
    void start(IUserEventsHandler& handler);
    void stop();

    void showNotConnected() override;
    void showConnecting() override;
    void showConnected() override;
    void showSmsNotification() override;
    void markSmsAsFailed(common::PhoneNumber to) override;
    void showCallMode(common::PhoneNumber from) override;
    void addReceivedSms(common::PhoneNumber from, const std::string& text) override;
    void showIncomingCall(common::PhoneNumber from) override;
    void showOutgoingCall(common::PhoneNumber to) override;
    void showUnknownRecipientOfCall(common::PhoneNumber to) override;
    void showIncomingCallTalk(common::PhoneNumber from, const std::string& text) override;

    void showSmsComposeMode();
    void showSmsList();
    void showDialMode();
    void showSms(const Sms& sms);
    common::PhoneNumber getCallPartnerNumber() override;
private:
    common::PrefixedLogger logger;
    IUeGui& gui;
    common::PhoneNumber phoneNumber;
    common::PhoneNumber callPartnerNumber;
    ISmsDb& smsDb;
    SmsService smsService;
    IUserEventsHandler* handler = nullptr;
};

}
