#pragma once

#include "Messages/BtsId.hpp"
#include "Messages/PhoneNumber.hpp"

namespace ue
{

class IBtsEventsHandler
{
public:
    virtual ~IBtsEventsHandler() = default;

    virtual void handleSib(common::BtsId) = 0;
    virtual void handleAttachAccept() = 0;
    virtual void handleAttachReject() = 0;
    virtual void handleDisconnected() = 0;
    virtual void handleSmsReceive(common::PhoneNumber, const std::string&) = 0;
    virtual void handleUnknownRecipientOfSms(common::PhoneNumber) = 0;
    virtual void handleCallAccept(common::PhoneNumber) = 0;
    virtual void handleCallRequest(common::PhoneNumber) = 0;
    virtual void handleUnknownRecipientOfCall(common::PhoneNumber) = 0;
    virtual void handleCallDrop(common::PhoneNumber) = 0;
    virtual void handleCallTalkReceive(common::PhoneNumber, const std::string&) = 0;
};

class IBtsPort
{
public:
    virtual ~IBtsPort() = default;

    virtual void sendAttachRequest(common::BtsId) = 0;
    virtual void sendSms(common::PhoneNumber, const std::string&) = 0;
    virtual void sendCallRequest(common::PhoneNumber) = 0;
    virtual void dropCall(common::PhoneNumber) = 0;
    virtual void acceptCall(common::PhoneNumber) = 0;
    virtual void sendCallTalk(common::PhoneNumber, const std::string&) = 0;
};

}
