#include "UserPort.hpp"
#include "UeGui/IListViewMode.hpp"
#include "UeGui/ISmsComposeMode.hpp"
#include "UeGui/IDialMode.hpp"
#include "UeGui/ITextMode.hpp"
#include "UeGui/ICallMode.hpp"
#include <iomanip>
#include <sstream>
#include <ctime>

namespace ue
{

UserPort::UserPort(common::ILogger &logger, IUeGui &gui, common::PhoneNumber phoneNumber, ISmsDb& smsDb)
    : logger(logger, "[USER-PORT]"),
      gui(gui),
      phoneNumber(phoneNumber),
      smsDb(smsDb),
      smsService(logger)
{}

void UserPort::start(IUserEventsHandler &handler)
{
    this->handler = &handler;
    gui.setTitle("Nokia " + to_string(phoneNumber));
}

void UserPort::stop()
{
    handler = nullptr;
}

void UserPort::showNotConnected()
{
    gui.showNotConnected();
}

void UserPort::showConnecting()
{
    gui.showConnecting();
}

void UserPort::showConnected()
{
    IUeGui::IListViewMode& menu = gui.setListViewMode();
    menu.clearSelectionList();
    menu.addSelectionListItem("Compose SMS", "");
    menu.addSelectionListItem("View SMS", "");
    menu.addSelectionListItem("Call", "");
    gui.setAcceptCallback([this,&menu](){
        const auto& [isSelected,index] = menu.getCurrentItemIndex();
        if(isSelected)
            switch(index)
            {
            case 0:
                showSmsComposeMode();
                break;
            case 1:
                showSmsList();
                break;
            case 2:
                showDialMode();
                break;
            default:
                logger.logError("Invalid index");
            }
    });
    gui.setRejectCallback([this](){
        logger.logDebug("Reject Clicked");
    });
}

void UserPort::showSmsComposeMode()
{
    IUeGui::ISmsComposeMode& smsComposer = gui.setSmsComposeMode();
    smsComposer.clearSmsText();
    smsComposer.clearPhoneNumber();
    gui.setAcceptCallback([this, &smsComposer](){
            const PhoneNumber& enteredPhoneNumber = smsComposer.getPhoneNumber();
            const std::string& enteredText = smsComposer.getSmsText();

            logger.logDebug("Sending SMS to: ", enteredPhoneNumber);
            smsDb.addSentSms(phoneNumber, enteredPhoneNumber, enteredText);
            handler->handleSmsSend(enteredPhoneNumber, enteredText);

            smsComposer.clearSmsText();
            smsComposer.clearPhoneNumber();
            showConnected();
         });
    gui.setRejectCallback([this, &smsComposer](){
            smsComposer.clearSmsText();
            smsComposer.clearPhoneNumber();
            showConnected();
        });
}

void UserPort::showSmsList()
{
    IUeGui::IListViewMode& menu = gui.setListViewMode();
    menu.clearSelectionList();
    std::stringstream s;
    for(Sms& sms : smsDb.getReceivedMessages()){
        s << std::left << std::setw(4) << common::to_string(sms.from) << std::setw(9) << sms.text.substr(0,8) << (sms.status==Status::notRead?"New":"");
        std::time_t t = std::chrono::system_clock::to_time_t(sms.timestamp);
        menu.addSelectionListItem(s.str(),std::ctime(&t));
        s.str("");
        s.clear();
    }
    gui.setAcceptCallback([this, &menu](){
            const auto& [isSelected,index] = menu.getCurrentItemIndex();
            if(isSelected){
                std::vector<Sms>& receivedMessages = smsDb.getReceivedMessages();
                Sms& sms = receivedMessages[index];
                if(sms.status == Status::notRead)
                {
                    smsService.markAsRead(sms);
                    auto notReadSms = std::find_if(std::begin(receivedMessages),
                                                   std::end(receivedMessages),
                                                   [](const Sms& sms){
                                                        return sms.status == Status::notRead;
                                                    });
                    if(notReadSms == std::end(receivedMessages))
                        gui.hideNewSms();
                }
                showSms(sms);
            }
        });
    gui.setRejectCallback([this](){
            showConnected();
        });
}

void UserPort::showDialMode()
{
    IUeGui::IDialMode& dialMode = gui.setDialMode();
    gui.setAcceptCallback([this, &dialMode](){
        const PhoneNumber& enteredPhoneNumber = dialMode.getPhoneNumber();
        callPartnerNumber = enteredPhoneNumber;
        logger.logDebug("Sending call request to: ", enteredPhoneNumber);
        handler->handleCallRequestSend(enteredPhoneNumber);
        dialMode.clearPhoneNumber();
        showOutgoingCall(enteredPhoneNumber);
    });
    gui.setRejectCallback([this, &dialMode](){
        dialMode.clearPhoneNumber();
        showConnected();
    });
}

void UserPort::showSms(const Sms& sms)
{
    IUeGui::ITextMode& smsView = gui.setViewTextMode();
    std::time_t t = std::chrono::system_clock::to_time_t(sms.timestamp);
    smsView.setText(common::to_string(sms.from) + "\n" + std::ctime(&t) + "\n" + sms.text);
    gui.setAcceptCallback([this](){
            logger.logDebug("Accept clicked");
        });
    gui.setRejectCallback([this](){
            showSmsList();
        });
}

void UserPort::showSmsNotification()
{
    gui.showNewSms();
}

void UserPort::markSmsAsFailed(common::PhoneNumber to)
{
    logger.logDebug("UserPort -> markSmsAsFailed");
    std::vector<Sms>& sentMessages = smsDb.getSentMessages();
    auto failedSms = std::find_if(std::rbegin(sentMessages),
                               std::rend(sentMessages),
                               [to](const Sms& sms){
                                    return sms.to == to && sms.status == Status::sent;
                                });
    if(failedSms != std::rend(sentMessages))
        smsService.markAsFailed(*failedSms);
    else
        logger.logError("Unexpected markSmsAsFailed");
}


void UserPort::addReceivedSms(common::PhoneNumber from, const std::string& text)
{
    logger.logDebug("UserPort -> addReceivedSms");
    smsDb.addReceivedSms(from, phoneNumber, text);
}

void UserPort::showCallMode(common::PhoneNumber from)
{
    IUeGui::ICallMode& callGui = gui.setCallMode();

    gui.setAcceptCallback([this, from, &callGui](){
            logger.logDebug("Sending msg");
            std::string msg = callGui.getOutgoingText();
            handler->handleCallTalkSend(from,msg);
            callGui.clearOutgoingText();
            callGui.appendIncomingText("Me:\n" + msg +'\n');
        });
    gui.setRejectCallback([this, from](){
            logger.logDebug("Call ended by user");
            handler->handleUserEndCall(from);
        });
}

void UserPort::showIncomingCall(common::PhoneNumber from){
    callPartnerNumber = from;
    gui.setAlertMode().setText("Incoming Call: " + common::to_string(from));
    gui.setAcceptCallback([this,from](){
        handler->handleIncomingCallAccept(from);
    });
    gui.setRejectCallback([this,from](){
        handler->handleIncomingCallDrop(from);
        showConnected();
    });
}

void UserPort::showOutgoingCall(common::PhoneNumber to)
{
    gui.setAlertMode().setText("Outgoing Call: " + common::to_string(to));
    gui.setAcceptCallback([this](){
        logger.logDebug("Accept clicked");
    });
    gui.setRejectCallback([this, to](){
        handler->handleOutgoingCallDrop(to);
        showConnected();
    });
}

void UserPort::showUnknownRecipientOfCall(common::PhoneNumber to)
{
    auto backToMenu = [this]() { handler->handleCancelUnknownRecipientShow();showConnected(); };
    gui.showPeerUserNotAvailable(to);
    gui.setAcceptCallback(backToMenu);
    gui.setRejectCallback(backToMenu);
}

void UserPort::showIncomingCallTalk(common::PhoneNumber from, const std::string& text)
{
    IUeGui::ICallMode& callGui = gui.setCallMode();
    callGui.appendIncomingText(common::to_string(from) +":\n" + text + "\n");
}

common::PhoneNumber UserPort::getCallPartnerNumber()
{
    return callPartnerNumber;
}

}
