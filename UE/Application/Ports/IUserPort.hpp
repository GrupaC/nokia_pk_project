#pragma once
#include "Messages/PhoneNumber.hpp"

namespace ue
{

class IUserEventsHandler
{
public:
    virtual ~IUserEventsHandler() = default;
    virtual void handleSmsSend(common::PhoneNumber, const std::string&) = 0;
    virtual void handleCallRequestSend(common::PhoneNumber) = 0;
    virtual void handleIncomingCallDrop(common::PhoneNumber) = 0;
    virtual void handleIncomingCallAccept(common::PhoneNumber) = 0;
    virtual void handleCancelUnknownRecipientShow() = 0;
    virtual void handleOutgoingCallDrop(common::PhoneNumber) = 0;
    virtual void handleUserEndCall(common::PhoneNumber) = 0;
    virtual void handleCallTalkSend(common::PhoneNumber, const std::string&) = 0;
};

class IUserPort
{
public:
    virtual ~IUserPort() = default;

    virtual void showNotConnected() = 0;
    virtual void showConnecting() = 0;
    virtual void showConnected() = 0;
    virtual void showSmsNotification() = 0;
    virtual void markSmsAsFailed(common::PhoneNumber) = 0;
    virtual void addReceivedSms(common::PhoneNumber, const std::string&) = 0;
    virtual void showCallMode(common::PhoneNumber) = 0;
    virtual void showIncomingCall(common::PhoneNumber) = 0;
    virtual void showOutgoingCall(common::PhoneNumber) = 0;
    virtual void showUnknownRecipientOfCall(common::PhoneNumber) = 0;
    virtual void showIncomingCallTalk(common::PhoneNumber, const std::string&) = 0;
    virtual common::PhoneNumber getCallPartnerNumber() = 0;
};

}
