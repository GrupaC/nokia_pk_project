#include "BtsPort.hpp"
#include "Messages/IncomingMessage.hpp"
#include "Messages/OutgoingMessage.hpp"

namespace ue
{

BtsPort::BtsPort(common::ILogger &logger, common::ITransport &transport, common::PhoneNumber phoneNumber)
    : logger(logger, "[BTS-PORT]"),
      transport(transport),
      phoneNumber(phoneNumber)
{}

void BtsPort::start(IBtsEventsHandler &handler)
{
    transport.registerMessageCallback([this](BinaryMessage msg) {handleMessage(msg);});
    transport.registerDisconnectedCallback([this]() {handleDisconnected();});
    this->handler = &handler;
}

void BtsPort::stop()
{
    transport.registerMessageCallback(nullptr);
    transport.registerDisconnectedCallback(nullptr);
    handler = nullptr;
}

void BtsPort::handleMessage(BinaryMessage msg)
{
    try
    {
        common::IncomingMessage reader{msg};
        auto msgId = reader.readMessageId();
        auto from = reader.readPhoneNumber();
        auto to = reader.readPhoneNumber();

        switch (msgId)
        {
        case common::MessageId::Sib:
        {
            auto btsId = reader.readBtsId();
            handler->handleSib(btsId);
            break;
        }
        case common::MessageId::AttachResponse:
        {
            bool accept = reader.readNumber<std::uint8_t>() != 0u;
            if (accept)
                handler->handleAttachAccept();
            else
                handler->handleAttachReject();
            break;
        }
        case common::MessageId::Sms:
        {
            auto encryptionType = reader.readNumber<std::uint8_t>();
            std::string text;
            switch(encryptionType)
            {
                case 0: //no encryption
                {
                    text = reader.readRemainingText();
                    break;
                }
                default:
                logger.logError("not implemented or wrong SMS encryption type");
            }
            handler->handleSmsReceive(from, text);
            break;
        }
        case common::MessageId::UnknownRecipient:
        {
            auto failingMsgId = reader.readMessageId();
            auto failingMsgFrom = reader.readPhoneNumber();
            auto failingMsgTo = reader.readPhoneNumber();

            switch (failingMsgId)
            {
            case common::MessageId::Sms:
            {
                logger.logDebug("Unknown recipient of SMS: ", failingMsgTo);
                handler->handleUnknownRecipientOfSms(failingMsgTo);
                break;
            }
            case common::MessageId::CallAccepted:
            case common::MessageId::CallDropped:
            case common::MessageId::CallRequest:
            {
                logger.logDebug("Unknown recipient of CallAccepted or CallDropped: ", failingMsgTo);
                handler->handleUnknownRecipientOfCall(failingMsgTo);
                break;
            }
            default:
                logger.logError("unknown failing message: ", failingMsgId);
            }
            break;
        }
        case common::MessageId::CallRequest:
        {
            auto encryptionType = reader.readNumber<std::uint8_t>();
            handler->handleCallRequest(from);
            break;
        }
        case common::MessageId::CallAccepted:
        {
            handler->handleCallAccept(from);
            break;
        }
        case common::MessageId::CallDropped:
        {
            handler->handleCallDrop(from);
            break;
        }
        case common::MessageId::CallTalk:
        {
            std::string text = reader.readRemainingText();
            handler->handleCallTalkReceive(from, text);
            break;
        }
        default:
            logger.logError("unknown message: ", msgId, ", from: ", from);
        }
     }
    catch (std::exception const& ex)
    {
        logger.logError("handleMessage error: ", ex.what());
    }
}


void BtsPort::sendAttachRequest(common::BtsId btsId)
{
    logger.logDebug("sendAttachRequest: ", btsId);
    common::OutgoingMessage msg{common::MessageId::AttachRequest,
                                phoneNumber,
                                common::PhoneNumber{}};
    msg.writeBtsId(btsId);
    transport.sendMessage(msg.getMessage());
}

void BtsPort::sendSms(common::PhoneNumber recipient, const std::string& text)
{
    logger.logDebug("sendSms: ", recipient);
    common::OutgoingMessage msg{common::MessageId::Sms,
                                phoneNumber,
                                recipient};

    msg.writeNumber<std::uint8_t>(0); // no encryption, TODO: other encryption methods
    msg.writeText(text);
    transport.sendMessage(msg.getMessage());
}

void BtsPort::dropCall(common::PhoneNumber to)
{
    logger.logDebug("dropCall: ", to);
    common::OutgoingMessage msg{common::MessageId::CallDropped,
                                phoneNumber,
                                to};
    transport.sendMessage(msg.getMessage());
}

void BtsPort::acceptCall(common::PhoneNumber to)
{
    logger.logDebug("acceptCall: ", to);
    common::OutgoingMessage msg{common::MessageId::CallAccepted,
                                phoneNumber,
                                to};
    transport.sendMessage(msg.getMessage());
}

void BtsPort::handleDisconnected()
{
    logger.logDebug("handleDisconnected");
    handler->handleDisconnected();
}

void BtsPort::sendCallTalk(common::PhoneNumber to, const std::string& text)
{
    logger.logDebug("sendCallTalk: ", to);
    common::OutgoingMessage msg{common::MessageId::CallTalk,
                               phoneNumber,
                               to};
    msg.writeText(text);
    transport.sendMessage(msg.getMessage());
}

void BtsPort::sendCallRequest(common::PhoneNumber to)
{
    logger.logDebug("sendCallRequest: ", to);
    common::OutgoingMessage msg{common::MessageId::CallRequest,
                                phoneNumber,
                                to};

    msg.writeNumber<std::uint8_t>(0); // no encryption, TODO: other encryption methods
    transport.sendMessage(msg.getMessage());
}

}
