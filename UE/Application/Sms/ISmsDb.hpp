#pragma once
#include "Messages/PhoneNumber.hpp"
#include "Sms.hpp"
#include <vector>

namespace ue
{
    using common::PhoneNumber;
    class ISmsDb
    {
    public:
        virtual ~ISmsDb() = default;

        virtual void addReceivedSms(PhoneNumber sender,
                                    PhoneNumber recipient,
                                    const std::string& text) = 0;

        virtual void addSentSms(PhoneNumber sender,
                                PhoneNumber recipient,
                                const std::string& text) = 0;

        virtual std::vector<Sms>& getReceivedMessages() = 0;
        virtual std::vector<Sms>& getSentMessages() = 0;

    };

}
