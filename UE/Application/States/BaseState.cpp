#include "BaseState.hpp"

namespace ue
{

BaseState::BaseState(Context &context, const std::string &name)
    : context(context),
      logger(context.logger, "[" + name + "]")
{
    logger.logDebug("entry");
}

BaseState::~BaseState()
{
    logger.logDebug("exit");
}

void BaseState::handleAttachTimeout()
{
    logger.logError("Uexpected: handleAttachTimeout");
}

void BaseState::handleIncomingCallTimeout()
{
    logger.logError("Uexpected: handleIncomingCallTimeout");
}

void BaseState::handleOutgoingCallTimeout()
{
    logger.logError("Uexpected: handleOutgoingCallTimeout");
}

void BaseState::handleUnknownRecipientTimeout()
{
    logger.logError("Uexpected: handleUnknownRecipientTimeout");
}

void BaseState::handleSib(common::BtsId btsId)
{
    logger.logError("Uexpected: handleSib: ", btsId);
}

void BaseState::handleAttachAccept()
{
    logger.logError("Uexpected: handleTimeout");
}

void BaseState::handleAttachReject()
{
    logger.logError("Uexpected: handleAttachReject");
}

void BaseState::handleDisconnected(){
    logger.logError("Unexpected: handleDisconnected");
}

void BaseState::handleSmsReceive(common::PhoneNumber from, const std::string& text)
{
    logger.logError("Unexpected: handleSmsReceive: ", from, "  ", text);
}

void BaseState::handleSmsSend(common::PhoneNumber to, const std::string& text)
{
    logger.logError("Unexpected: handleSmsSend: ", to, "  ", text);
}

void BaseState::handleUnknownRecipientOfSms(common::PhoneNumber to)
{
    logger.logError("Unexpected: handleUnknownRecipientOfSms: ", to);
}

void BaseState::handleCallAccept(common::PhoneNumber from)
{
    logger.logError("Unexpected: handleCallAccept", from);
}

void BaseState::handleCallDrop(common::PhoneNumber from)
{
    logger.logError("Unexpected: handleCallDrop", from);
}

void BaseState::handleCallRequestSend(common::PhoneNumber to)
{
    logger.logError("Unexpected:: handleCallRequestSend: ", to);
}

void BaseState::handleCallRequest(common::PhoneNumber from)
{
        logger.logError("Unexpected: handleCallRequest: ", from);
}

void BaseState::handleIncomingCallDrop(common::PhoneNumber to)
{
    logger.logError("Unexpected: handleIncomingCallDrop: ", to);
}

void BaseState::handleIncomingCallAccept(common::PhoneNumber to)
{
    logger.logError("Unexpected: handleIncomingCallAccept: ", to);
}

void BaseState::handleUnknownRecipientOfCall(common::PhoneNumber to)
{
    logger.logError("Unexpected: handleUnknownRecipientOfCall: ", to);
}

void BaseState::handleCancelUnknownRecipientShow()
{
    logger.logError("Unexpected: handleCancelUnknownRecipientShow");
}

void BaseState::handleOutgoingCallDrop(common::PhoneNumber to)
{
    logger.logError("Unexpected: handleOutgoingCallDrop: ", to);
}

void BaseState::handleUserEndCall(common::PhoneNumber from)
{
    logger.logError("Unexpected: handleUserEndCall: ", from);
}

void BaseState::handleCallTalkSend(common::PhoneNumber to, const std::string& text)
{
    logger.logError("Unexpected: handleCallTalkSend: ", to);
}

void BaseState::handleTalkingTimeout()
{
    logger.logError("Unexpected: handleTalkingTimeout: ");
}

void BaseState::handleCallTalkReceive(common::PhoneNumber from, const std::string& text)
{
    logger.logError("Unexpected: handleCallTalkReceive: ", from);
}

}
