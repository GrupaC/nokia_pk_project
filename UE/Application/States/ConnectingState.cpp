#include "ConnectingState.hpp"
#include "ConnectedState.hpp"
#include "NotConnectedState.hpp"

namespace ue
{

ConnectingState::ConnectingState(Context &context, common::BtsId btsId)
    : BaseState(context, "ConnectingState")
{
    context.bts.sendAttachRequest(btsId);
    context.user.showConnecting();
    context.timer.startAttachTimer();
}

void ConnectingState::handleAttachAccept()
{
    logger.logDebug("ConnectingState -> handleAttachAccept");
    context.timer.stopAttachTimer();
    context.setState<ConnectedState>();
}

void ConnectingState::handleAttachReject()
{
    logger.logDebug("ConnectingState -> handleAttachReject");
    context.timer.stopAttachTimer();
    context.setState<NotConnectedState>();
}

void ConnectingState::handleAttachTimeout()
{
    logger.logDebug("ConnectingState -> handleTimeout");
    context.setState<NotConnectedState>();
}

void ConnectingState::handleDisconnected()
{
    logger.logDebug("ConnectingState -> handleDisconnected");
    context.timer.stopAttachTimer();
    context.setState<NotConnectedState>();
}

}
