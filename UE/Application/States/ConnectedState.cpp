#include "ConnectedState.hpp"
#include "NotConnectedState.hpp"
#include "TalkingState.hpp"

namespace ue
{

ConnectedState::ConnectedState(Context &context)
    : BaseState(context, "ConnectedState")
{
    context.user.showConnected();
}

void ConnectedState::handleIncomingCallTimeout()
{
    logger.logDebug("ConnectedState -> handleIncomingCallTimeout");
    context.user.showConnected();
    context.bts.dropCall(context.user.getCallPartnerNumber());
}

void ConnectedState::handleOutgoingCallTimeout()
{
    logger.logDebug("ConnectedState -> handleOutgoingCallTimeout");
    context.user.showConnected();
    context.bts.dropCall(context.user.getCallPartnerNumber());
}

void ConnectedState::handleUnknownRecipientTimeout()
{
    logger.logDebug("ConnectedState -> handleUnknownRecipientTimeout");
    context.user.showConnected();
}

void ConnectedState::handleDisconnected()
{
    logger.logDebug("ConnectedState -> handleDisconnected");
    context.setState<NotConnectedState>();
}

void ConnectedState::handleSmsReceive(common::PhoneNumber from, const std::string& text)
{
    logger.logDebug("ConnectedState -> handleSmsReceive: ", from);
    context.user.addReceivedSms(from, text);
    context.user.showSmsNotification();
}

void ConnectedState::handleSmsSend(common::PhoneNumber to, const std::string& text)
{
    logger.logDebug("ConnectedState -> sendSms to: ", to);
    context.bts.sendSms(to, text);
}

void ConnectedState::handleUnknownRecipientOfSms(common::PhoneNumber to)
{
    logger.logDebug("ConnectedState -> handleUnknownRecipientOfSms: ", to);
    context.user.markSmsAsFailed(to);
}

void ConnectedState::handleCallAccept(common::PhoneNumber from)
{
    logger.logDebug("ConnectedState -> handleCallAccept: ", from);
    context.user.showCallMode(from);
    context.timer.stopOutgoingCallTimer();
    context.setState<TalkingState>();
}

void ConnectedState::handleCallDrop(common::PhoneNumber from)
{
    logger.logDebug("ConnectedState -> handleCallDrop", from);
    context.timer.stopIncomingCallTimer();
    context.user.showConnected();
}

void ConnectedState::handleCallRequestSend(common::PhoneNumber to)
{
    logger.logDebug("ConnectedState -> handleCallRequestSend: ", to);
    context.timer.startOutgoingCallTimer();
    context.bts.sendCallRequest(to);
}

void ConnectedState::handleCallRequest(common::PhoneNumber from)
{
    logger.logDebug("ConnectedState -> handleCallRequest: ", from);
    context.user.showIncomingCall(from);
    context.timer.startIncomingCallTimer();
}

void ConnectedState::handleIncomingCallDrop(common::PhoneNumber to)
{
    logger.logDebug("ConnectedState -> handleIncomingCallDrop: ", to);
    context.timer.stopIncomingCallTimer();
    context.bts.dropCall(to);
}

void ConnectedState::handleIncomingCallAccept(common::PhoneNumber to)
{
    logger.logDebug("ConnectedState -> handleIncomingCallAccept: ", to);
    context.timer.stopIncomingCallTimer();
    context.bts.acceptCall(to);
    context.user.showCallMode(to);
    context.setState<TalkingState>();
}

void ConnectedState::handleUnknownRecipientOfCall(common::PhoneNumber to)
{
    logger.logDebug("ConnectedState -> handleUnknownRecipientOfCall: ", to);
    context.timer.startUnknownRecipientTimer();
    context.user.showUnknownRecipientOfCall(to);
}

void ConnectedState::handleOutgoingCallDrop(common::PhoneNumber to)
{
    logger.logDebug("ConnectedState -> handleOutgoingCallDrop: ", to);
    context.timer.stopIncomingCallTimer();
    context.bts.dropCall(to);
}

}
