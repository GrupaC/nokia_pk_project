#pragma once

#include "BaseState.hpp"

namespace ue
{

class TalkingState : public BaseState
{
public:
    TalkingState(Context& context);

    // ITimerEventsHandler interface
    void handleUnknownRecipientTimeout() override;
    void handleTalkingTimeout() override;

    // IBtsEventsHandler interface
    void handleUnknownRecipientOfCall(common::PhoneNumber to) override;
    void handleCallDrop(common::PhoneNumber from) override;
    void handleCallTalkReceive(common::PhoneNumber from, const std::string& text) override;

    // IUserEventsHandler interface
    void handleCancelUnknownRecipientShow() override;
    void handleUserEndCall(common::PhoneNumber from) override;
    void handleCallTalkSend(common::PhoneNumber to, const std::string& text) override;
};

}
