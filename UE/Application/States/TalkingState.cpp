#include "TalkingState.hpp"
#include "ConnectedState.hpp"

namespace ue
{

TalkingState::TalkingState(Context &context)
    : BaseState(context, "TalkingState")
{

}

void TalkingState::handleUnknownRecipientOfCall(common::PhoneNumber to)
{
    logger.logDebug("TalkingState -> handleUnknownRecipientOfCall: ", to);
    context.timer.startUnknownRecipientTimer();
    context.user.showUnknownRecipientOfCall(to);
}

void TalkingState::handleUnknownRecipientTimeout()
{
    logger.logDebug("TalkingState -> handleTimeout");
    context.setState<ConnectedState>();
}

void TalkingState::handleCancelUnknownRecipientShow()
{
    logger.logDebug("TalkingState -> handleCancelUnknownRecipientShow");
    context.timer.stopUnknownRecipientTimer();
    context.setState<ConnectedState>();
}

void TalkingState::handleUserEndCall(common::PhoneNumber from)
{
    logger.logDebug("TalkingState -> handleUserEndCall");
    context.bts.dropCall(from);
    context.timer.stopTalkingTimer();
    context.setState<ConnectedState>();
}

void TalkingState::handleCallDrop(common::PhoneNumber from)
{
    logger.logDebug("TalkingState -> handleCallDrop: ", from);
    context.timer.stopTalkingTimer();
    context.setState<ConnectedState>();
}

void TalkingState::handleCallTalkSend(common::PhoneNumber to, const std::string& text)
{
    logger.logDebug("TalkingState -> handleUserEndCall");
    context.timer.stopTalkingTimer();
    context.timer.startTalkingTimer();
    context.bts.sendCallTalk(to, text);
}

void TalkingState::handleCallTalkReceive(common::PhoneNumber to, const std::string& text)
{
    logger.logDebug("TalkingState -> handleCallTalkReceive");
    context.timer.stopTalkingTimer();
    context.timer.startTalkingTimer();
    context.user.showIncomingCallTalk(to, text);
}

void TalkingState::handleTalkingTimeout()
{
    logger.logDebug("TalkingState -> handleTalkingTimeout");
    context.bts.dropCall(context.user.getCallPartnerNumber());
    context.setState<ConnectedState>();
}


}
