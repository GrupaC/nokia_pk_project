#pragma once

#include "BaseState.hpp"

namespace ue
{

class ConnectedState : public BaseState
{
public:
    ConnectedState(Context& context);
    void handleIncomingCallTimeout() override;
    void handleOutgoingCallTimeout() override;
    void handleUnknownRecipientTimeout() override;
    void handleDisconnected() override;
    void handleSmsReceive(common::PhoneNumber from, const std::string& text) override;
    void handleSmsSend(common::PhoneNumber to, const std::string& text) override;
    void handleUnknownRecipientOfSms(common::PhoneNumber to) override;
    void handleCallAccept(common::PhoneNumber from) override;
    void handleCallDrop(common::PhoneNumber from) override;
    void handleCallRequestSend(common::PhoneNumber to) override;
    void handleCallRequest(common::PhoneNumber from) override;
    void handleIncomingCallDrop(common::PhoneNumber to) override;
    void handleIncomingCallAccept(common::PhoneNumber to) override;
    void handleUnknownRecipientOfCall(common::PhoneNumber to) override;
    void handleOutgoingCallDrop(common::PhoneNumber to) override;
};

}
