#pragma once

#include "Logger/PrefixedLogger.hpp"
#include "Messages/PhoneNumber.hpp"
#include "IEventsHandler.hpp"
#include "Context.hpp"
#include <mutex>

namespace ue
{

using common::PhoneNumber;
using common::ILogger;

class Application : public IEventsHandler
{
public:
    Application(PhoneNumber phoneNumber,
                ILogger& iLogger,
                IBtsPort& bts,
                IUserPort& user,
                ITimerPort& timer);
    ~Application();

    // ITimerEventsHandler interface
    void handleAttachTimeout() override;
    void handleIncomingCallTimeout() override;
    void handleOutgoingCallTimeout() override;
    void handleUnknownRecipientTimeout() override;
    void handleTalkingTimeout() override;

    // IBtsEventsHandler interface
    void handleSib(common::BtsId btsId) override;
    void handleAttachAccept() override;
    void handleAttachReject() override;
    void handleDisconnected() override;
    void handleSmsReceive(common::PhoneNumber from, const std::string& text);
    void handleUnknownRecipientOfSms(common::PhoneNumber to) override;
    void handleCallAccept(common::PhoneNumber from) override;
    void handleCallRequest(common::PhoneNumber from) override;
    void handleUnknownRecipientOfCall(common::PhoneNumber to) override;
    void handleCallDrop(common::PhoneNumber from) override;
    void handleCallTalkReceive(common::PhoneNumber from, const std::string& text) override;

    //IUserEventsHandler interface
    void handleSmsSend(common::PhoneNumber to, const std::string& text) override;
    void handleCallRequestSend(common::PhoneNumber to) override;
    void handleIncomingCallDrop(common::PhoneNumber to) override;
    void handleIncomingCallAccept(common::PhoneNumber to) override;
    void handleCancelUnknownRecipientShow() override;
    void handleOutgoingCallDrop(common::PhoneNumber to) override;
    void handleUserEndCall(common::PhoneNumber from) override;
    void handleCallTalkSend(common::PhoneNumber to, const std::string& text) override;

private:
    Context context;
    common::PrefixedLogger logger;
    std::mutex mut;

};

}
