#include "Timer.hpp"

Timer::Timer() : running(false)
{
}

Timer::Timer(Duration dur, Callback clbk)
    : running(false), clbk(clbk), dur(dur)
{
}

void Timer::start()
{
    tp = Clock::now() + dur;
    running = true;
}

void Timer::stop()
{
    running = false;
}

void Timer::runCallback()
{
    clbk();
}

bool Timer::isRunning()
{
    return running;
}

Timer::TimePoint Timer::getTimePoint()
{
    return tp;
}
