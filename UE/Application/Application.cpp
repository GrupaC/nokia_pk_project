#include "Application.hpp"
#include "States/NotConnectedState.hpp"
#include <mutex>

namespace ue
{

Application::Application(common::PhoneNumber phoneNumber,
                         common::ILogger &iLogger,
                         IBtsPort &bts,
                         IUserPort &user,
                         ITimerPort &timer)
    : context{iLogger, bts, user, timer},
      logger(iLogger, "[APP] ")
{
    logger.logInfo("Started");
    context.setState<NotConnectedState>();
}

Application::~Application()
{
    logger.logInfo("Stopped");
}

void Application::handleAttachTimeout()
{
    std::lock_guard<std::mutex> l(mut);
    context.state->handleAttachTimeout();
}

void Application::handleIncomingCallTimeout()
{
    std::lock_guard<std::mutex> l(mut);
    context.state->handleIncomingCallTimeout();
}

void Application::handleOutgoingCallTimeout()
{
    std::lock_guard<std::mutex> l(mut);
    context.state->handleOutgoingCallTimeout();
}

void Application::handleUnknownRecipientTimeout()
{
    std::lock_guard<std::mutex> l(mut);
    context.state->handleUnknownRecipientTimeout();
}

void Application::handleSib(common::BtsId btsId)
{
    std::lock_guard<std::mutex> l(mut);
    context.state->handleSib(btsId);
}

void Application::handleAttachAccept()
{
    std::lock_guard<std::mutex> l(mut);
    context.state->handleAttachAccept();
}

void Application::handleAttachReject()
{
    std::lock_guard<std::mutex> l(mut);
    context.state->handleAttachReject();
}

void Application::handleDisconnected()
{
    std::lock_guard<std::mutex> l(mut);
    context.state->handleDisconnected();
}

void Application::handleSmsReceive(common::PhoneNumber from, const std::string& text)
{
    std::lock_guard<std::mutex> l(mut);
    context.state->handleSmsReceive(from, text);
}

void Application::handleSmsSend(common::PhoneNumber to, const std::string& text)
{
    std::lock_guard<std::mutex> l(mut);
    context.state->handleSmsSend(to, text);
}

void Application::handleUnknownRecipientOfSms(common::PhoneNumber to)
{
    std::lock_guard<std::mutex> l(mut);
    context.state->handleUnknownRecipientOfSms(to);
}

void Application::handleCallAccept(common::PhoneNumber from)
{
    std::lock_guard<std::mutex> l(mut);
    context.state->handleCallAccept(from);
}
void Application::handleCallDrop(common::PhoneNumber from)
{
    std::lock_guard<std::mutex> l(mut);
    context.state->handleCallDrop(from);
}
void Application::handleCallRequestSend(common::PhoneNumber to)
{
    std::lock_guard<std::mutex> l(mut);
    context.state->handleCallRequestSend(to);
}

void Application::handleCallRequest(common::PhoneNumber from)
{
    std::lock_guard<std::mutex> l(mut);
    context.state->handleCallRequest(from);
}

void Application::handleIncomingCallDrop(common::PhoneNumber to)
{
    std::lock_guard<std::mutex> l(mut);
    context.state->handleIncomingCallDrop(to);
}

void Application::handleIncomingCallAccept(common::PhoneNumber to)
{
    std::lock_guard<std::mutex> l(mut);
    context.state->handleIncomingCallAccept(to);
}

void Application::handleUnknownRecipientOfCall(common::PhoneNumber to)
{
    std::lock_guard<std::mutex> l(mut);
    context.state->handleUnknownRecipientOfCall(to);
}

void Application::handleCancelUnknownRecipientShow()
{
    std::lock_guard<std::mutex> l(mut);
    context.state->handleCancelUnknownRecipientShow();
}

void Application::handleOutgoingCallDrop(common::PhoneNumber to)
{
    std::lock_guard<std::mutex> l(mut);
    context.state->handleOutgoingCallDrop(to);
}

void Application::handleUserEndCall(common::PhoneNumber from)
{
    std::lock_guard<std::mutex> l(mut);
    context.state->handleUserEndCall(from);
}

void Application::handleCallTalkSend(common::PhoneNumber to, const std::string& text)
{
    std::lock_guard<std::mutex> l(mut);
    context.state->handleCallTalkSend(to, text);
}

void Application::handleTalkingTimeout()
{
    std::lock_guard<std::mutex> l(mut);
    context.state->handleTalkingTimeout();
}

void Application::handleCallTalkReceive(common::PhoneNumber from, const std::string& text)
{
    std::lock_guard<std::mutex> l(mut);
    context.state->handleCallTalkReceive(from, text);
}

}
